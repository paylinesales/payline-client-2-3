import React, { useState, useEffect } from "react";

import BusinessCategoryContext from "./BusinessCategoryContext";
import { PAYMENT_TYPES, BUSINESS_TYPES } from "../constants";

type providerProps = {
  slug: string | Array<string> | undefined;
};

// Declare the possible types to verify against.

const { SMALL_BUSINESS } = BUSINESS_TYPES;
const { ONLINE } = PAYMENT_TYPES;

// Create a component to pass the context data through.

const BusinessCategoryProvider: React.FC<providerProps> = (props) => {
  const { slug, children } = props;

  const [businessType, setBusinessType] = useState(SMALL_BUSINESS);
  const [paymentType, setPaymentType] = useState(ONLINE);

  useEffect(() => {
    const [business, payment] = slug || [];
    setBusinessType(business);
    setPaymentType(payment);
  }, [slug]);

  // Return the provider
  return (
    <BusinessCategoryContext.Provider
      key="businessCategory"
      value={{
        slug,
        businessType,
        setBusinessType,
        paymentType,
        setPaymentType,
      }}>
      {children}
    </BusinessCategoryContext.Provider>
  );
};

export default BusinessCategoryProvider;
