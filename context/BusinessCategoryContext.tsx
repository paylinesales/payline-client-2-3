import React, { useContext } from "react";
// import { businessCategoryType } from "@/utils/getBackgroundImages";
import { PAYMENT_TYPES, BUSINESS_TYPES } from "../constants";

const { SMALL_BUSINESS } = BUSINESS_TYPES;
const { ONLINE } = PAYMENT_TYPES;

type contextValue = {
  toggleBusinessType?: () => void;
  // checkBusinessCategory: (businessType: string, paymentType: string) => boolean;
  slug: string | Array<string> | undefined;
  businessType: string;
  paymentType: string;
  setBusinessType: (businessType: string) => void;
  setPaymentType: (paymentType: string) => void;
};
// Set "Small Business Online" as the default page.
const defaultValue: contextValue = {
  toggleBusinessType: () => {
    return null;
  },
  // checkBusinessCategory: () => {
  //   return false;
  // },
  slug: undefined,
  businessType: SMALL_BUSINESS,
  setBusinessType: () => null,
  paymentType: ONLINE,
  setPaymentType: () => null,
};

const BusinessCategoryContext = React.createContext(defaultValue);

export const useBusinessCategory = () => {
  return useContext(BusinessCategoryContext);
};

export default BusinessCategoryContext;
