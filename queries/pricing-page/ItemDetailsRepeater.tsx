import { gql } from "@apollo/client";

const repeaterQuery = gql`
  query repeaterQuery {
    pages(where: { title: "Interchange Rate & Pricing" }) {
      edges {
        node {
          interchangeRate {
            cardAccordion {
              accordion {
                cardLabelRepeater {
                  cardLabel
                  cardTypeRepeater {
                    cardType
                    fieldGroupName
                    cardTypeDetails {
                      fieldGroupName
                      itemDetailsText
                      priceDetails
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`;

export default repeaterQuery;
