import React from "react";
import NextLink, { LinkProps } from "next/link";
// Force Link Component Update
export interface Props extends LinkProps {
  className?: string;
  href: string;
}

const Link: React.FC<Props> = (props) => {
  const { className: classes, href, children } = props;
  return (
    <NextLink href={href}>
      <a className={classes}>{children}</a>
    </NextLink>
  );
};

export default Link;
