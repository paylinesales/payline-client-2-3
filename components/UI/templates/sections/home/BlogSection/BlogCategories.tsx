import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper.min.css";
// import Swiper core and required modules
import SwiperCore, { Navigation } from "swiper/core";
// Activate Swiper modules
SwiperCore.use([Navigation]);

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const BlogCategories: React.FC<{ className: string; categories: any }> = (
  props,
) => {
  const { className, categories } = props;
  const { edges } = categories;
  return (
    <div className={`flex flex-row gap-8 ${className}`}>
      <Swiper spaceBetween={30} slidesPerView="auto" className="w-full">
        {edges.map((category) => (
          <SwiperSlide className="category-slide my-4">
            <a
              href={`/blogs/category/${category.node.slug}`}
              className="py-2 px-8 rounded-2xl border-2 border-payline-border-light hover:border-payline-dark transition-colors text-payline-black">
              {category.node.name}
            </a>
          </SwiperSlide>
        ))}
        <SwiperSlide className="category-slide my-4">
          <a
            href="/blog"
            className="py-2 px-14 bg-payline-black border-2 border-payline-black rounded-2xl text-payline-white uppercase">
            More
          </a>
        </SwiperSlide>
      </Swiper>
    </div>
  );
};

export default BlogCategories;
