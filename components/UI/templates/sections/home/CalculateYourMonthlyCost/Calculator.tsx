import React, { useEffect, useState } from "react";
import { useBusinessCategory } from "context/BusinessCategoryContext";
import PaymentOptionsBanner from "components/UI/organisms/banners/PaymentOptionsBanner";
import Link from "components/UI/atoms/LinkComponent";
import { useRouter } from "next/dist/client/router";
import AvgTransactionAmt from "./AvgTransactionAmt";
import AvgMonthlyVolume from "./AvgMonthlyVolume";
import MonthlyCharges from "./MonthlyCharges";
import { PAYMENT_TYPES, BUSINESS_TYPES } from "../../../../../../constants";

const { SMALL_BUSINESS } = BUSINESS_TYPES;

const { ONLINE, IN_PERSON } = PAYMENT_TYPES;
const amounts = [25, 50, 100, 175, 250, 500, 1000];

const Calculator: React.FC = () => {
  const [enteredAvgTransactionAmt, setEnteredAvgTransactionAmt] = useState(0);
  const [enteredAvgMonthlyVolume, setEnteredAvgMonthlyVolume] = useState(0);
  const [businessType, setBusinessType] = useState(IN_PERSON);
  const { paymentType, slug } = useBusinessCategory();
  const router = useRouter();
  // for as long as the payment type is on line then the business is online
  const isOnlineBusiness = paymentType === ONLINE;
  const isUserInputComplete =
    enteredAvgTransactionAmt !== 0 && enteredAvgMonthlyVolume !== 0;
  const isSmallBusiness = businessType === SMALL_BUSINESS;
  const businessSlug = isSmallBusiness ? "/small-business" : "/enterprise";

  useEffect(() => {
    if (isOnlineBusiness) {
      setBusinessType(ONLINE);
    } else {
      setBusinessType(IN_PERSON);
    }
  }, [isOnlineBusiness]);

  const calculatorBtnText = () => {
    let text;
    if (isOnlineBusiness) {
      text = "Eligible for a month free.";
    } else {
      text = "Eligible for instant approval.";
    }
    return text;
  };

  const avgTransactonAmtHandler = (
    event: React.MouseEvent<HTMLButtonElement>,
  ) => {
    const buttonValue =
      event.currentTarget.dataset.value === undefined
        ? 0
        : +event.currentTarget.dataset.value;
    setEnteredAvgTransactionAmt(buttonValue);
  };

  const avgMonthlyVolumeHandler = (
    event: React.ChangeEvent<HTMLInputElement>,
  ) => {
    const inputValue =
      event.target.value === undefined ? 0 : +event.target.value;
    setEnteredAvgMonthlyVolume(inputValue);
  };

  const monthlyCost = (
    avgTransactionAmount: number,
    avgMonthlyVolume: number,
  ) => {
    if (enteredAvgTransactionAmt === 0 || enteredAvgMonthlyVolume === 0) {
      return 0;
    }
    // (monthly volume *2.1%) + ((monthly volume / average transaction amount) *$0.20) + $20 -> Online
    // (monthly volume *1.9%) + ((monthly volume / average transaction) *$0.10) + $10 -> In person

    let monthlyCostValue;
    if (businessType === ONLINE) {
      monthlyCostValue =
        avgMonthlyVolume * 0.019 +
        (avgMonthlyVolume / avgTransactionAmount) * 0.2 +
        20;
    } else {
      monthlyCostValue =
        avgMonthlyVolume * 0.016 +
        (avgMonthlyVolume / avgTransactionAmount) * 0.1 +
        10;
    }
    return monthlyCostValue;
  };

  const businessTypeHandler = (event: React.MouseEvent<HTMLButtonElement>) => {
    const activeBusinessType = event.currentTarget.dataset.type;
    setBusinessType(activeBusinessType!);
    if (slug !== undefined) {
      router.push(
        `${businessSlug}/${activeBusinessType}`,
        `${businessSlug}/${activeBusinessType}`,
        {
          scroll: false,
        },
      );
    }
  };

  return (
    <div
      id="monthly-cost-calculator"
      className="flex flex-wrap rounded bg-payline-white filter drop-shadow-none lg:drop-shadow-lg mt-14 mb-10">
      <div className="w-full lg:w-7/12 p-9">
        <PaymentOptionsBanner
          className="flex flex-col"
          businessType={businessType}
          businessTypeHandler={businessTypeHandler}>
          <p className="text-md text-payline-black mb-4 uppercase lg:normal-case">
            My business takes payments...
          </p>
        </PaymentOptionsBanner>
        <p className="text-md text-payline-black my-4 uppercase lg:normal-case">
          Average transaction amount:
        </p>
        <AvgTransactionAmt
          amounts={amounts}
          onChange={avgTransactonAmtHandler}
          activeValue={enteredAvgTransactionAmt}
        />
        <AvgMonthlyVolume onChange={avgMonthlyVolumeHandler} />
      </div>
      <div className="w-full lg:w-5/12 bg-payline-background-light border-t-2 lg:border-0 border-payline-border-light  pb-10 lg:pb-0">
        <h3 className="text-lg text-payline-black font-bold text-3xl pt-9 px-9">
          Monthly Charges:
        </h3>
        <MonthlyCharges
          value={monthlyCost(enteredAvgTransactionAmt, enteredAvgMonthlyVolume)}
        />
      </div>
      {isUserInputComplete && (
        <Link
          href="https://paylinedata.com/apply-now-ue/"
          className="w-full bg-payline-cta text-white py-6 rounded-b text-center text-2xl font-bold">
          <span className="hidden lg:inline-block">
            {calculatorBtnText()} &nbsp;
          </span>
          Apply now!
        </Link>
      )}
    </div>
  );
};

export default Calculator;
