import React from "react";
import { useRouter } from "next/dist/client/router";
import Image from "next/dist/client/image";
import { PRODUCT_TYPES } from "../../../../../../constants";

const PlansIcon: React.FC = () => {
  const router = useRouter();
  const { productType } = router?.query ?? {};
  const iconSrc =
    productType === PRODUCT_TYPES.SOFTWARE
      ? "/images/svg/money-hand-green.svg"
      : "/images/svg/money-hand.svg";
  return <Image src={iconSrc} width={41} height={40} />;
};

export default PlansIcon;
