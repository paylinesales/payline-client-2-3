import React, { ReactElement, useState } from "react";
import Image from "next/image";
import Link from "next/link";
import moment from "moment";
import { ChevronRightIcon } from "@heroicons/react/outline";
import WhiteButton from "@/components/UI/atoms/WhiteButton";
import mod from "../../../../../helpers/mod";
import SearchInput from "../../../atoms/SearchInput";

const FirstBlogPost = ({ post }) => {
  return (
    <div className="h-600 mx-6 py-5 flex flex-col hover:shadow-md overflow-hidden w-96">
      <div className="flex-shrink-0 mx-auto">
        {post.node.featuredImage ? (
          <img
            className="w-full object-none"
            src={post.node.featuredImage.node.sourceUrl}
            alt=""
          />
        ) : (
          <img
            className="object-contain object-center"
            // src="/images/ach.png"
            src={Math.random() > 0.5 ? "/images/ach.png" : "/images/blog1.png"}
            alt=""
          />
        )}
      </div>
      <div className="flex-1 bg-white p-6 flex flex-col justify-between">
        <p>{moment(post.node.date).format("DD MMMM YYYY")}</p>
        <div className="flex-1">
          <Link href={`/blog/${post.node.slug}`}>
            <div className="block mt-2 cursor-pointer">
              <p className="text-4xl font-semibold text-gray-900 line-clamp-3">
                {post.node.title}
              </p>
            </div>
          </Link>
        </div>
      </div>
      <div>
        <Link href={`/blog/${post.node.slug}`}>
          <div className="cursor-pointer flex gap-3 text-payline-black font-bold px-6 bg-white">
            <div>Read</div>
            <Image
              src="/images/svg/chevron-right-black.svg"
              width={8}
              height={15}
            />
          </div>
        </Link>
      </div>
    </div>
  );
};

const BlogPost = ({ post }) => {
  return (
    <div className="h-400 mx-6 py-5 mb-6 flex flex-col flex-grow-0 max-height-3/4 hover:shadow-md items-start overflow-hidden w-52">
      <div className="flex-shrink-0">
        {post.node.featuredImage ? (
          <img
            className="h-96 w-full object-cover"
            src={post.node.featuredImage.node.sourceUrl}
            alt=""
          />
        ) : (
          <img
            className="object-contain object-center"
            src={
              Math.random() > 0.5 ? "/images/blog2.png" : "/images/blog3.png"
            }
            alt=""
          />
        )}
      </div>
      <div className=" bg-white p-6 flex flex-col justify-between">
        <p>{moment(post.node.date).format("DD MMMM YYYY")}</p>
        <div className="flex-1">
          <Link href={`/blog/${post.node.slug}`}>
            <div className="cursor-pointer block mt-2">
              <p className="text-xl font-semibold text-gray-900 line-clamp-3">
                {post.node.title}
              </p>
            </div>
          </Link>
        </div>
      </div>
      <div>
        <Link href={`/blog/${post.node.slug}`}>
          <a className="cursor-pointer flex gap-2 items-center text-payline-black font-bold px-6">
            <span>Read</span>
            <ChevronRightIcon className="w-5 h-5" />
          </a>
        </Link>
      </div>
    </div>
  );
};

const BlogPosts = ({ posts }) => {
  const [start, setStart] = useState(0);
  return (
    <>
      <div className="mx-6 my-3 text-payline-black text-lg font-semibold">
        RECENT ARTICLES{" "}
      </div>
      <div className="my-3 gap-16 mx-auto flex">
        {posts
          .slice(start, start + 4)
          .map((post, index) =>
            index === 0 ? (
              <FirstBlogPost post={post} key={post.node.slug} />
            ) : (
              <BlogPost post={post} key={post.node.slug} />
            ),
          )}
      </div>
      <div className="my-12 px-32 mx-auto flex justify-center gap-6">
        <WhiteButton
          className=""
          onClick={() => setStart((s) => mod(s - 1, 500))}
          disabled={start === 0}>
          <Image
            src="/images/svg/chevron-left-black.svg"
            width={8}
            height={15}
          />
        </WhiteButton>
        <WhiteButton
          className=""
          onClick={() => setStart((s) => mod(s + 1, 500))}
          disabled={start === posts.length - 4}>
          <Image
            src="/images/svg/chevron-right-black.svg"
            width={8}
            height={15}
          />
        </WhiteButton>
      </div>
    </>
  );
};

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
function RecentArticlesSection({ postsData }): ReactElement {
  const { edges } = postsData;

  return (
    <>
      <div className="hidden md:block bg-blog bg-no-repeat pl-12">
        <div className="px-0 pt-10 gap-5 flex flex-row header justify-around w-full">
          <div className="my-12 max-w-7xl">
            <p className="text-6xl font-hkgrotesk leading-tight font-bold text-payline-black">
              The
              <span className="text-6xl px-2 font-hkgrotesk leading-tight font-bold text-payline-white bg-payline-cta">
                Blog
              </span>
            </p>
          </div>

          <div className="flex items-center">
            <SearchInput />
          </div>

          <div className="flex">
            <Image src="/images/svg/blog-woman.svg" width={72} height={72} />
            <div className="flex items-center -ml-5">
              <button
                type="button"
                className="flex items-center gap-3 px-8 rounded-full bg-payline-dark text-white text-payline-dark p-3">
                <div>Request a Callback</div>
                <div>
                  <Image
                    src="/images/svg/chevron-right.svg"
                    width={6}
                    height={12}
                  />
                </div>
              </button>
            </div>
          </div>
        </div>
        <BlogPosts posts={edges} />
      </div>
    </>
  );
}

export default RecentArticlesSection;
