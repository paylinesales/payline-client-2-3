import React from "react";

import Image from "next/dist/client/image";
import Link from "../../atoms/LinkComponent";

const NavLogo: React.FC = () => {
  return (
    <div className="logo pr-8">
      <Link href="/">
        <a>
          <Image
            src="/images/payline-logo.svg"
            alt="Payline"
            height={30}
            width={100}
          />
        </a>
      </Link>
    </div>
  );
};

export default NavLogo;
