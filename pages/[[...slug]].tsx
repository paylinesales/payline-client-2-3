/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import React from "react";
import { useBusinessCategory } from "context/BusinessCategoryContext";
import client from "lib/apolloClient";
import BlogLayout from "@/components/UI/layouts/BlogLayout";
import LandingPage from "@/components/UI/templates/LandingPage/LandingTemplate";
import websiteCopyQuery from "../queries/websiteCopy";
import landingPageQuery from "../queries/landingPages";

interface PageProps {
  websiteCopyData: string;
  smallBusinessOnlineData: string;
  smallBusinessInPersonData: string;
  enterpriseInPersonData: string;
  enterpriseOnlineData: string;
}

let landingPageData = "";

const Home: React.FC<PageProps> = ({
  websiteCopyData,
  smallBusinessOnlineData,
  smallBusinessInPersonData,
  enterpriseOnlineData,
  enterpriseInPersonData,
}) => {
  let websiteCopyDataParsed = JSON.parse(websiteCopyData);
  const { paymentType, businessType } = useBusinessCategory();
  websiteCopyDataParsed = websiteCopyDataParsed.data.websiteCopy;

  // Determine the state of the app and render the data
  if (paymentType === "online" && businessType === "enterprise") {
    landingPageData = enterpriseOnlineData;
    // return data for small business online
  } else if (paymentType === "in-person" && businessType === "enterprise") {
    landingPageData = enterpriseInPersonData;
  } else if (paymentType === "in-person" && businessType === "small-business") {
    landingPageData = smallBusinessInPersonData;
  } else {
    landingPageData = smallBusinessOnlineData;
  }
  return (
    <BlogLayout>
      <LandingPage
        websiteCopyData={websiteCopyDataParsed}
        landingPageData={landingPageData}
      />
    </BlogLayout>
  );
};

export async function getServerSideProps() {
  // Handle state & context updates here
  // When the URL changes update the request from the app.
  // fetch data for each landing page

  const websiteCopy = await client.query({
    query: websiteCopyQuery,
  });

  const smallBusinessOnline = await client.query({
    query: landingPageQuery,
    variables: {
      title: "Small Business Online",
    },
  });

  const smallbusinessInPerson = await client.query({
    query: landingPageQuery,
    variables: {
      title: "Small Business In-Person",
    },
  });

  const enterpriseInPerson = await client.query({
    query: landingPageQuery,
    variables: {
      title: "Enterprise In-Person",
    },
  });

  const enterpriseOnline = await client.query({
    query: landingPageQuery,
    variables: {
      title: "Enterprise Online",
    },
  });

  // const calculatorData = JSON.stringify(calculator);
  const websiteCopyData = JSON.stringify(websiteCopy);
  const smallBusinessOnlineData = JSON.stringify(smallBusinessOnline);
  const smallBusinessInPersonData = JSON.stringify(smallbusinessInPerson);
  const enterpriseOnlineData = JSON.stringify(enterpriseOnline);
  const enterpriseInPersonData = JSON.stringify(enterpriseInPerson);
  return {
    props: {
      websiteCopyData,
      smallBusinessOnlineData,
      smallBusinessInPersonData,
      enterpriseInPersonData,
      enterpriseOnlineData,
    },
  };
}

export default Home;
